package NewTestCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;


public class TC001_CreateLead extends ProjectMethods
{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new l ead";
		category = "smoke";
		author = "Alban";
		excelFileName = "CLPages";
	}


	@Test (dataProvider = "fetchdata")
	public void createNewLead( String UName, String Pwd, String CName, String FName, String LName) 
	{
		new LoginPage()
		.typeUserName(UName)
		.typePassword(Pwd)
		.clicklogin()
		.LoginName("Welcome Demo Sales Manager")
		.clickCrmSfa()
		.clickLead()
		.clickCreateLead()
		.enterCName(CName)
		.enterFName(FName)
		.enterLName(LName)
		.clickSubmit();


	}
}
